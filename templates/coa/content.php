
<div class="mui-row">
   <div class="mui-col-sm-10 mui-col-sm-offset-1">
    <br>
    <div class="mui--text-dark-secondary mui--text-body2"><h1><?php echo $page->getMeta('title'); ?></h1></div>
    <div class="mui-divider"></div>
    <br>
		<h3><?php echo $page->getMeta('problema_solucionado'); ?></h3>
		<?php echo $page->getContent(); ?>
    <br>
  </div>
</div>
