<?php
// Coa main layout.
?>

<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//cdn.muicss.com/mui-0.4.1/css/mui.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $site['base_domain']; ?>templates/coa/css/coa.css" rel="stylesheet" type="text/css" />
    <script src="//cdn.muicss.com/mui-0.4.1/js/mui.min.js"></script>
    <style>
     /* Tutorial CSS goes here */
    </style>
  </head>
  <body>
	<div id="sidebar">
	  <div class="mui--text-light mui--text-display1 mui--align-vertical">Sotol CPA</div>
	</div>
	<div id="content" class="mui-container-fluid">
	  <?php echo $content; ?>
	</div>
  </body>
</html>