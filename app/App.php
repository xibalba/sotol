<?php
/**
 * @copyright	2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/sotol
 */

namespace xibalba\sotol;

defined('SOTOL_APP_DIR') || define('SOTOL_APP_DIR', __DIR__);
defined('SOTOL_BASE_DIR') || define('SOTOL_BASE_DIR', realpath(__DIR__ . '/../'));

use xibalba\sotol\controller\Distpatcher;
use xibalba\sotol\http\Request;
use xibalba\sotol\model\Page;

use xibalba\ocelote\traits\ConfigAware;
use xibalba\ocelote\ArrayHelper;

use xibalba\ocelote\Bag;

use phpFastCache\CacheManager as PhpFastCache;
use phpFastCache\Exceptions\phpFastCacheDriverCheckException;
use phpFastCache\Drivers\Files\Driver as FileDriver;

/**
 * Application class.
 * This class catch the app config, init the application, catch the request and dispatch a response.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class App {
	use ConfigAware;

	protected $_request;

	protected $_currentPage;

	protected $_cacheEngine;

	/**
	 * Holds the flats.
	 *
	 * To access the data for a specific page, use the url as key: $this->siteCollection['url'].
	 *
	 * @var xibalba\ocelote\Bag
	 */
	protected $_flatCollecion;

	/**
	 * @type \xibalba\sotol\App
	 */
	protected static $_instance = null;

	/**
	 * Return singleton application object.
	 *
	 * @return \xibalba\sotol\App singleton application object.
	 */
	public static function getInstance(array $config = []) : App {
		if(static::$_instance === null) {
			if(empty($config)) throw new \Exception("Fist call for singleton must include config array.");
			static::$_instance = new static($config);
		}
		return static::$_instance;
	}

	/**
	* Construct an Application object.
	*
	* @throws \Exception
	*/
	protected function __construct(array $config) {
		$this->configure($config);
		$this->_flatCollecion = new Bag();
	}

	private function configure(array $config) {
		if(!ArrayHelper::keyExists('content_dir', $config)) $config['content_dir'] = SOTOL_BASE_DIR . '/flats';
		else $config['content_dir'] = SOTOL_BASE_DIR . '/' . $config['content_dir'];

		$templateConfig = $config['template'] ?? [];
		$templateConfig['dir'] = SOTOL_BASE_DIR . '/' . ArrayHelper::getValue($templateConfig, 'dir', 'templates');

		$config['template'] = $templateConfig;

		if($cacheConfig = ArrayHelper::getValue($config, 'cache', false)) {
			if(ArrayHelper::getValue($cacheConfig, 'active', false)) {
				$cacheSettings = $cacheConfig['config'] ?? [];
				$cacheSettings['path'] = $cacheSettings['path'] ?? SOTOL_BASE_DIR . '/cache';
				$cacheSettings['securityKey'] = $cacheSettings['securityKey'] ?? '';
				
				try {
					PhpFastCache::setDefaultConfig($cacheSettings);
					$this->_cacheEngine = PhpFastCache::getInstance('files');
				}
				catch(phpFastCacheDriverCheckException $e) {
					// @todo throw a «SotolException»
					$message = ' Cannot setup cache. Ensure cache directory is accessible.';
					throw new \Exception($e->getMessage() . $message, $e->getCode());
				}
			}
		}

		$this->setConfig($config);
	}

	/**
	 * Execute the Sotol instance and call the distpatcher.
	 */
	public function run() {
		(new Distpatcher($this->getConfig('content_dir'), $this->getConfig('template')))->distpatch();
	}

	/**
	 * Set the collection for the app.
	 *
	 * @param Bag $collecion The collection of flat files content.
	 */
	public function setFlatCollection(Bag $collecion) {
		$this->_flatCollecion = $collecion;
	}

	/**
	 * @return the Bag of flat files collection.
	 */
	public function getFlatCollection() : Bag {
		return $this->_flatCollecion;
	}

	/**
	 * Set the current page object for the request.
	 */
	public function setCurrentPage(Page $page) {
		$this->_currentPage = $page;
	}

	/**
	 * @return Page The current page object for the request.
	 */
	public function getCurrentPage() : Page {
		return $this->_currentPage;
	}

	/**
	 * @return FileDriver | false whenever the cache engine if is active.
	 */
	public function getCacheEngine() {
		if($this->_cacheEngine instanceof FileDriver) return $this->_cacheEngine;
		else return false;
	}

	/**
	 * @return Request The actual request object.
	 */
	public function getRequest() : Request {
		if(!$this->_request instanceof Request) $this->_request = new Request();
		return $this->_request;
	}
}