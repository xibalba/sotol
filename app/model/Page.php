<?php
/**
 * @copyright 	2016 Xibalbá Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link 		https://gitlab.com/xibalba/sotol
 */

namespace xibalba\sotol\model;

use xibalba\sotol\App;

use xibalba\sotol\util\Parser;
use xibalba\sotol\controller\Paginator;

use xibalba\ocelote\Bag;

/**
 * Page class
 * This class holds all attributes of a single page.
 * A Page object describes a procesed piece of content,
 * that is to say a valid «.md» file ready to be used on templates (or anywhere).
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Page {
	protected $_slug;

	protected $_url;

	protected $_parentUrl;

	protected $_meta;

	protected $_content;

	protected $_paginator;

	/**
	 * Create a new Page object.
	 *
	 * @param string $file Filename to load content and create the page.
	 * @param string $parentUrl Parent URL respect to actual page.
	 */
	public function __construct(string $file, string $parentUrl) {
		$this->_meta = new Bag();

		$data = Parser::flatMdFile($file);

		$this->_meta->add($data['meta']);
		$this->_content = $data['content'];
		$this->_parentUrl = $parentUrl;
		$this->_slug = $this->_parentUrl . basename($file, '.md');
		$this->_url = App::getInstance()->getConfig('site')['base_domain'].$this->_slug;
	}

	/**
	 * @return string The slug for the page.
	 */
	public function getSlug() : string {
		return $this->_slug;
	}

	/**
	 * @return string The URL of the page.
	 */
	public function getUrl() : string {
		return $this->_url;
	}

	/**
	 * Return a meta value for the passed key. A default value can be
	 * passed as optional if no data is found for the passed key.
	 *
	 * @param string $key The key to seek some value.
	 * @param mixed $default Optional value to return if no value is found for the passed key.
	 *
	 * @return mixed The value for the passed key.
	 */
	public function getMeta(string $key, $default = null) {
		return $this->_meta->get($key, $default);
	}

	/**
	 * @return string The content of the page.
	 */
	public function getContent() : string {
		return $this->_content;
	}

	/**
	 *@return string The parent URP respect to actuap page.
	 */
	public function getParentUrl() : string {
		return $this->_parentUrl;
	}

	/**
	 * @return Paginator A paginator object.
	 */
	public function getPaginator(string $path = '') : Paginator {
		if(empty($path)) $path = $this->_parentUrl;

		if($this->_paginator instanceof Paginator) {
			$this->_paginator->setPath($path);
			$this->_paginator->setNumberPage();
		}
		else $this->_paginator = new Paginator($path);

		return $this->_paginator;
	}
}
