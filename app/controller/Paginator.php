<?php
/**
 * @copyright	2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/sotol
 */

namespace xibalba\sotol\controller;

use xibalba\sotol\App;
use xibalba\sotol\model\Page;
use xibalba\sotol\util\Sort;

use xibalba\ocelote\Bag;
use xibalba\ocelote\StringHelper;
use xibalba\ocelote\Checker;

/**
 * Paginator class
 * This class provide pagination functionality
 * 
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Paginator {

	protected $_pagedLimit;

	protected $_currentPage;

	protected $_contentDir;

	protected $_path;

	protected $_collection;

	protected $_numberPage;

	protected $_filesOnPath = [];

	public function __construct(string $path) {
		$this->_contentDir = App::getInstance()->getConfig('content_dir');
		$this->_collection = App::getInstance()->getFlatCollection();
		$this->_pagedLimit = App::getInstance()->getConfig('paged_limit', 6);
		$this->_currentPage = App::getInstance()->getCurrentPage();
		$this->setPath($path);

		$this->setNumberPage();
	}

	/**
	 * Set the number page of actual pagination.
	 */
	public function setNumberPage($nbPage = null) {
		if(!is_int($nbPage)) $this->_numberPage = (isset($_GET['page'])) ? $_GET['page'] : 1;
		else $this->_numberPage = $nbPage;
	}

	/**
	 * @return int the actual number of pagination.
	 */
	public function getNumberPage() : int {
		return $this->_numberPage;
	}

	/**
	 * Restore the path to the parent path of the current page.
	 */
	public function resetPath() {
		$this->_path = App::getInstance()->getCurrentPage()->getParentUrl();
	}

	/**
	 * Set the path to the pagination.
	 * @param string $path The path to set.
	 */
	public function setPath(string $path) {
		if($path !== $this->_path) $this->_path = $path;
	}

	/**
	 * Collect the file structure for the passed path.
	 *
	 * @param string $path Path to collect.
	 * @param bool $recursive false to ignore internat directories, otherwie must be true.
	 */
	protected function collectDir(string $path, bool $recursive = false) {
		$neddle = $path.'*';
		$currentPageUrl = $this->_currentPage->getUrl();

		if($files = glob($this->_contentDir . $neddle . '[!index404]*.md')) {
			// This will check the files of directory, and if not exist on collection
			// then will be created.
			foreach($files as $fileName) { 
				// Clean dir names for set urlable
				$slug = basename($fileName, '.md');

				$url = $path.$slug;
				if($url == $currentPageUrl) continue;

				if(!$this->_collection->has($url)) {
					$page = new Page($fileName, $path);
					$this->_collection->set($url, $page);
				}

				$this->_filesOnPath[] = $url;
			}

		}

		if($recursive) {
			if($dirs = glob($this->_contentDir . $neddle, GLOB_ONLYDIR)) {
				foreach($dirs as $dir) $this->collectDir($path . basename($dir) . '/', true);
			}
		}
	}

	/**
	 * Return the previus number page for actual pagination.
	 * The result can be changed by a flag for ignore the internal subdirectories
	 * respect to the current path.
	 *
	 * @param bool $all
	 * @return int The result for the previus number page.
	 */
	public function getPagedPrev(bool $all = true) : int {
		if(Checker::isEmpty($this->_filesOnPath)) $this->collectDir($this->_path, $all);
		$actual = $this->getNumberPage();
		return --$actual;
	}

	/**
	 * Return the next number page for actual pagination.
	 * The result can be changed by a flag for ignore the internal subdirectories
	 * respect to the current path.
	 *
	 * @param bool $all
	 * @return int The result for the previus number page.
	 */
	public function getPagedNext(bool $all = true) : int {
		if(Checker::isEmpty($this->_filesOnPath)) $this->collectDir($this->_path, $all);
		$actual = $this->getNumberPage();
		if($actual <= $this->getPagedCount($all)) return ++$actual;
		return $this->getPagedCount();
	}

	/**
	 * Return the result of calculate the number of pages for current pagination
	 * in function of the configured itemes per page and the total of content
	 * for current path.
	 * The result can be changed by a flag for ignore the internal subdirectories
	 * respect to the current path.
	 *
	 * @param bool $all
	 * @return int The paged count result.
	 */
	public function getPagedCount(bool $all = true) : int {
		if(Checker::isEmpty($this->_filesOnPath)) $this->collectDir($this->_path, $all);
		return (int) ceil((count($this->_filesOnPath) / $this->_pagedLimit));
	}

	/**
	 * Return an array of collected pages for the actual page of pagination.
	 *
	 * @param array $options Options for pagination
	 * @param bool $all
	 *
	 * @return array the collected pages.
	 */
	public function getPaged(array $options = [], bool $all = true) : array {
		// Ensure collection
		$this->_filesOnPath = [];
		$this->collectDir($this->_path, $all);

		if(!Checker::isEmpty($this->_filesOnPath)) {
			$page = [];

			// Prepare
			foreach($this->_filesOnPath as $fileName) $page[] = $this->_collection->get($fileName);

			// Apply sort
			if(isset($options['sort'])) {
				$sortMethod = 'by';

				if($options['sort'] == 'date') $sortMethod .= 'Date';
				else $sortMethod .= 'Alpha';

				if(isset($options['order']) && $options['order'] == 'desc') $sortMethod .= 'Desc';
				else $sortMethod.= 'Asc';

				Sort::$sortMethod($page);
			}

			$numberPage = $this->_numberPage;
			if(isset($options['number_page'])) $numberPage = $options['number_page'];

			if($numberPage > 0) {
				// Lets pagin
				$extracts = [];

				$limit = $this->_pagedLimit;
				$offset = ($this->_numberPage - 1) * $limit;
				$absLimit = $offset + $limit;
				$pagesCount = count($page);
				if($absLimit > $pagesCount) $absLimit = $pagesCount;

				$pagin = [];
				for($i = $offset; $i < $absLimit; $i++) $pagin[] = $page[$i];

				return $pagin;
			}
			else return $page;
		}
	}
}