<?php
/**
 * @copyright	2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/sotol
 */

namespace xibalba\sotol\controller;

use xibalba\sotol\App;
use xibalba\sotol\model\Page;

use xibalba\sotol\util\traits\TemplateRenderer;

use xibalba\ocelote\ArrayHelper;
use xibalba\ocelote\Checker;
use xibalba\ocelote\Inflector;
use xibalba\ocelote\Converter;

use xibalba\ocelote\Bag;

/**
 * Distpatcher class.
 * This class implements the functionality for distpatch a page from an URL.
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Distpatcher {
	use TemplateRenderer;

	protected $_cacheLifeTime;

	protected $_contentDir;

	protected $_urlsParts;

	protected $_requestNeddle;

	/**
	 * Holds the flats.
	 *
	 * To access the data for a specific page, use the url as key: $this->siteCollection['url'].
	 *
	 * @var xibalba\ocelote\Bag
	 */
	protected $_flatCollecion;

	/**
	 * Create the Distpatcher object.
	 *
	 * @param string $contentDir Directory to seek content to distpatch.
	 * @param array $templateConfig Configuration array for template.
	 */
	public function __construct(string $contentDir, array $templateConfig) {
		$this->_contentDir = $contentDir;
		$this->_flatCollecion = new Bag();

		$this->setTemplatesPath($templateConfig['dir']);
		$this->setTemplateName(ArrayHelper::getValue($templateConfig, 'name', 'coa'));
	}

	/**
	 * @return int The lifetime in seconds for cache.
	 */
	protected function getChacheLifeTime() : int {
		if(Checker::isEmpty($this->_cacheLifeTime)) {
			$cacheConfig = App::getInstance()->getConfig('cache', []);
			$this->_cacheLifeTime = $cacheConfig['lifetime'] ?? 0;
		}

		return $this->_cacheLifeTime;
	}

	/**
	 * @return string The request string needle for seek content.
	 */
	protected function getRequestNeddle() : string {
		if(Checker::isEmpty($this->_requestNeddle)) {
			$request = App::getInstance()->getRequest();
			$url = $request->getUrl();

			$needle = $url;
			if($url == '/') $needle = '/index';

			// if exist params, atach to $needle
			if($request->getParams()->count() > 0) {
				$serialGet = Converter::toSerialize($request->getParams()->all());
				$needle .= '_serial_get_'.$serialGet;
			}

			$this->_requestNeddle = base64_encode($needle);
		}

		return $this->_requestNeddle;
	}


	/**
	 * The site structure is compose by all directories than contains an index.md file.
	 *
	 * This methods searches $path recursively for «index.md» files and adds the parsed data to $_flatCollection.
	 *
	 * After successful indexing, the $siteCollection holds basically all information (except media files)
	 * from all pages of the whole site.
	 * this makes searching and filtering very easy since all data is stored in one place.
	 * To access the data of a specific page within the $siteCollection array,
	 * the page's url serves as the key: $this->siteCollection['/path/to/page']
	 *
	 * When $_flatCollecion is empty this method must be called.
	 *
	 * @param string $path
	 */
	protected function collectSiteStructure(string $path = '/') {
		$indexNeddle = $this->_contentDir.$path.'index.md';

		$contentIndex = glob($indexNeddle);
		if(Checker::isArray($contentIndex) && !Checker::isEmpty($contentIndex)) {
			$page = new Page($contentIndex[0], $path);
			$this->addPage($page);

			if($dirs = glob($this->_contentDir . $path. '*', GLOB_ONLYDIR)) {
				// Sort $dirs array again to be independent from glob's default behavior in case of any inconsistency.
				sort($dirs);
				// Scan each directory recursively.
				foreach ($dirs as $dir) $this->collectSiteStructure($path . basename($dir) . '/');
			}
		}
	}

	/**
	 * Create a new Page object or return false when cannot create one.
	 *
	 * @param string $path Path to seek files iwth content.
	 * @param string $flatName Name of file to seek.
	 *
	 * @return Page | false Whenever was done.
	 */
	public function createPage(string $path, string $flatName) {
		$neddle = $path.$flatName.'.md';
		$files = glob($this->_contentDir . $neddle);

		if(!Checker::isEmpty($files)) {
			$page = new Page($files[0], $path);
			$this->addPage($page);
			return $page;
		}

		else return false;
	}

	/**
	 * Add a Page object to pages collection Bag.
	 *
	 * @param Page Page object to add.
	 */
	public function addPage(Page $page) {
		$request = App::getInstance()->getRequest();

		$idx = $page->getSlug();
		$requestUrl = $request->getUrl();
		if($requestUrl == '/') $requestUrl = '/index';

		if($request->getParams()->count() > 0 && $idx == $requestUrl) {
			$serialGet = Converter::toSerialize($request->getParams()->all());
			$idx .= '_serial_get_'.$serialGet;
		}

		$idx = base64_encode($idx);

		$this->_flatCollecion->set($idx, $page);
		$this->setPageOnCache($idx, $page);
	}

	/**
	 * Stores a Page on cache for the passed index.
	 *
	 * @param string $idx Index to store the page.
	 */
	public function setPageOnCache(string $idx, Page $page) {
		$cacheEngine = App::getInstance()->getCacheEngine();

		if($cacheEngine) {
			$cachedPage = $cacheEngine->getItem($idx);
			$cachedPage->set($page)->expiresAfter($this->getChacheLifeTime());
			$cacheEngine->save($cachedPage);
		}
	}

	/**
	 * Return a chached page or null if there is no one for the request needle.
	 */
	public function getPageFromCache() {
		$cacheEngine = App::getInstance()->getCacheEngine();
		if($cacheEngine) {
			$needle = $this->getRequestNeddle();

			$page = $cacheEngine->getItem($needle);
			if($page !== null) return $page->get();
		}

		return null;
	}

	/**
	 * Try to retrive a Page object from supported source (cache or site structure).
	 * If cannot create a Page by any way then return null.
	 */
	public function getPage() {
		// Try to get page from cache
		$page = $this->getPageFromCache();
		if($page !== null) return $page;

		// if page do not exist on cache, then check directory content and collect structure site
		if($this->_flatCollecion->count() == 0) {
			$contentDir = $this->_contentDir;
			if(!file_exists($contentDir)) throw new \Exception("Specified content flats directory «$contentDir» does not exist.");
			if(!is_readable($contentDir)) throw new \Exception("Specified content flats directory «$contentDir» is not readable.");

			$this->collectSiteStructure();
		}

		$request = App::getInstance()->getRequest();
		$url = $request->getUrl();
		$needle = $this->getRequestNeddle();

		// try to retrive from collection by url (this check for '/index' route)
		if($page = $this->_flatCollecion->get($needle)) return $page;
		// try to retrive concatenate '$needle' with  '/index'
		else if($page = $this->_flatCollecion->get(base64_encode(base64_decode($needle).'/index'))) return $page;
		// try to create
		else {
			$flatName = basename($url);
			if(Checker::isEmpty($flatName)) $flatName = 'index';
			$path = str_replace($flatName, '', $url);
			$indexNeedle = base64_encode($path.'index');

			// Check if exist on collection the path + index
			if($this->_flatCollecion->has($indexNeedle)) return $this->createPage($path, $flatName);
			else return null;
		}
	}

	/**
	 * Distpatch content for actual request.
	 */
	public function distpatch() {
		$cacheEngine = App::getInstance()->getCacheEngine();

		// First check if exist an html output for the passed url
		$output = null;
		if($cacheEngine) {
			$needle = $this->getRequestNeddle().'_html_outpul_';
			$cachedContent = $cacheEngine->getItem($needle);
			$output = $cachedContent->get();

			if($output !== null) {
				echo $output;
				return;
			}
		}

		// If no allowed cache or not found an output, lets get the Page.
		$page = $this->getPage();

		if($page instanceof Page) {
			App::getInstance()->setFlatCollection($this->_flatCollecion);
			App::getInstance()->setCurrentPage($page);

			$template = $page->getMeta('template', 'content');

			$output = $this->render($template, [
				'page' => $page,
				'site' => App::getInstance()->getConfig('site')
			]);

			if($cacheEngine) {
				$cachedContent = $cacheEngine->getItem($needle);
				$cachedContent->set($output)->expiresAfter($this->getChacheLifeTime());
				$cacheEngine->save($cachedContent);
			}

			echo $output;
		}
		else {
			echo $this->render('404', [
				'site' => App::getInstance()->getConfig('site')
			]);
		}
	}
}
