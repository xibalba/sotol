<?php
/**
 * @copyright	2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/sotol
 */

define('SOTOL_BASE_DIR', realpath(__DIR__ . '/../'));

return [
	'content_dir' => 'flats',
	'paged_limit' => 9,
	
	'site' => [
		'title' => 'Sotol Content Plublishing Application',
		'name' => 'Sotol CPA',
		
		'base_domain' => 'http://sotol.dev'
	],
	
	'template' => [
		'name' => 'coa'
	],
	
	'cache' => [
		'active' => false,
		'lifetime' => 90,
		'config' => [
			'path' => SOTOL_BASE_DIR . '/cache'
		]
	],
	
	'show_disqus' => false
];