<?php
/**
 * @copyright 	2016 Xibalbá Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link 		https://gitlab.com/xibalba/sotol
 */
 
namespace xibalba\sotol\http;

use xibalba\sotol\util\WrapperBag;
use xibalba\ocelote\Bag;

/**
 * Request class
 * This class provide a Request lite abstraction, wraps PHP global's arrays:
 *   $_COOKIE, $_FILES, $_GET, $_POST, $_SERVER.
 * Also provide basic methods for request manipulation.
 * 
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Request extends Bag {

	protected $_url;
	protected $_method;

	public function __construct() {
		$this->set('files', $_FILES);
		$this->set('cookies', new WrapperBag($_COOKIE));
		$this->set('get', new WrapperBag($_GET));
		$this->set('post', new WrapperBag($_POST));
		$this->set('server', new WrapperBag($_SERVER));
	}

	/**
	 * Return the wrapped $_GET content.
	 */
	public function getGet() {
		return $this->get('get');
	}

	/**
	 * getGet alias
	 */
	public function getParams() {
		return $this->getGet();
	}

	/**
	 * Return the wrapped $_POST content.
	 */
	public function getPost() {
		return $this->get('post');
	}

	/**
	 * Return the wrapped $_SERVER content.
	 */
	public function getServer() {
		return $this->get('server');
	}

	/**
	 * @return string The request method.
	 */
	public function getMethod() :string {
		if(!$this->_method) {
			$method = strtoupper($this->getServer()->get('REQUEST_METHOD', 'GET'));

			if($method === 'POST') {
				if($override = $this->getServer()->get('HTTP_X_METHOD_OVERRIDE')) $method = strtoupper($override);
			}

			$this->setMethod($method);
		}

		return $this->_method;
	}

	/**
	 * @return string The request URL.
	 */
	public function getUrl() : string {
		if (!$this->_url) {
			$server = $this->getServer();
			$script = str_replace($server->get('DOCUMENT_ROOT'), '', $server->get('SCRIPT_FILENAME'));
			$base = dirname($script);
			$url = '/';

			// Proper URL defined by the web server
			if($path = $server->get('PATH_INFO')) $url = $path;
			// Strip off the query string if it exists
			else if ($path = $server->get('REQUEST_URI')) $url = explode('?', $path)[0];
			// Remove the base folder and index file
			else if ($path = $server->get('PHP_SELF')) $url = str_replace($script, '', $path);

			if ($base !== '/' && $base !== '\\') $url = $base . $url;
			if(strlen($url) > 1) $url = rtrim($url, '\\/');

			$this->setUrl($url);
		}

		return $this->_url;
	}

	/**
	 * Set the request method.
	 *
	 * @param string $method Tha method string to set.
	 * @return Request the request instance.
	 */
	public function setMethod(string $method) : Request {
		$method = strtoupper($method);

		// if(!in_array($method, Http::getMethodTypes())) throw new \Exception(sprintf('Invalid method %s', $method));

		$this->_method = $method;
		$this->getServer()->set('REQUEST_METHOD', $method);

		return $this;
	}

	/**
	 * Set the request URL string.
	 *
	 * @param string $url The URL string to set.
	 * @return Request the request instance.
	 */
	public function setUrl(string $url) : Request {
		$this->_url = $url;
		return $this;
	}
}