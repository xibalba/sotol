<?php
/**
 * @copyright	2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link 		https://gitlab.com/xibalba/sotol
 */

namespace xibalba\sotol\util;

use xibalba\ocelote\Bag as BaseBag;

/**
 * WrapperBag class
 * This class extends from ocelote Bag and allow to be build with a given array to constructor.
 * 
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class WrapperBag extends BaseBag {
	/**
	 * Set the parameters.
	 *
	 * @param array $data
	 */
	public function __construct(array $data = []) {
		$this->_data = $data;
	}
}