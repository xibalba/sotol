<?php
/**
 * @copyright	2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link		https://gitlab.com/xibalba/sotol
 */

 namespace xibalba\sotol\util\traits;
 
/**
 * Provide functionality for render html from php templates
 *
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
trait TemplateRenderer {
	/**
	 * @var string Store the name for the wrapper to use on template
	 */
	protected $_layout = 'main';

	/**
	 * @var string Store the directoy names where to find layouts. All layouts
	 * must be inside this direvtory.
	 */
	protected $_layoutDirName = 'layouts';

	/**
	 * @var string Store the path where to find the templates.
	 */
	protected $_templatesPath = '';

	/**
	 * @var string Store the name of template
	 */ 
	protected $_templateName = 'coa';
	
	/**
	 * Set the templates path where to find templates
	 *
	 * @param string $path Path of the views
	 */
	protected function setTemplatesPath(string $path) {
		$this->_templatesPath = $path;
	}

	/**
	 * Set the directory name for the layout
	 *
	 * @param string $dirName the layout directory name to set.
	 */
	protected function setLayoutDirName(string $dirName) {
		$this->_layoutDirName = $dirName;
	}

	/**
	 * Set the directory name for view.
	 *
	 * @param string $dirName the view directory name to set.
	 */
	protected function setTemplateName(string $templateName) {
		$this->_templateName = $templateName;
	}

	/**
	 * Returns the layout file name.
	 *
	 * @throws Exception if no layout has set,
	 * @return string The layout file name.
	 */
	protected function getLayoutFileName() : string {
		if(empty($this->_layout)) throw new \Exception('No layout is set');
		return $this->_layout.'.php';
	}

	/**
	 * Returns the directory name for search the layout file.
	 *
	 * @throws Exception if no layout directory name has set.
	 * @return string The firectory name for search the layout file.
	 */
	protected function getLayoutDirName() : string {
		if(empty($this->_layoutDirName)) throw new \Exception('No layout directory name is set.');
		return $this->_layoutDirName;
	}

	/**
	 * Return a processed html from the passed view name.
	 *
	 * @param string $view name of the template to use.
	 * @param array $params Params for pass to the view template.
	 *
	 * @return string html result.
	 */
	protected function getTemplate(string $view, array $params = []) : string {
		$templateFile = $this->getTemplatesPath().'/'.$this->getTemplateName().'/'.$view.'.php';
		return $this->renderFile($templateFile, $params);
	}

	/**
	 * Returns the base view path for search view files.
	 *
	 * @throws Exception if no view path has set.
	 * @return string
	 */
	protected function getTemplatesPath() : string {
		if(empty($this->_templatesPath)) throw new \Exception('No view path is set.');
		return $this->_templatesPath;
	}

	/**
	 * Returns the template name
	 *
	 * @return string Template name.
	 */
	protected function getTemplateName() : string {
		if(empty($this->_templateName)) return '';
		return $this->_templateName;
	}

	/**
	 * Compose a html document with the passed view template and the configured layout
	 * and return that.
	 *
	 * @param string $view Name of the view to render
	 * @param array $params Params for pass to the view
	 */
	public function render(string $view, array $params = []) : string {
		$layoutFile = $this->getTemplatesPath().'/'.$this->getTemplateName().'/'.$this->getLayoutDirName().'/'.$this->getLayoutFileName();
		$params['content'] = $this->getTemplate($view, $params);
		
		return $this->renderFile($layoutFile, $params);
	}

	/**
	 * Return a html from the passed view file.
	 *
	 * @param string $viewFile Full route and file name of the view to use.
	 * @param array $params Params for pass to the view.
	 *
	 * @throws Exception if view file cannot be found.
	 *
	 * @return string Html result.
	 */
	public function renderFile(string $viewFile, array $params = []) : string {
		if(!file_exists($viewFile)) throw new \Exception(sprintf('Template %s file does not exist.', $viewFile));

		ob_start();
		if(!empty($params)) extract($params);
		require($viewFile);

		return ob_get_clean();
	}
}