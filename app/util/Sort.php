<?php
/**
 * @copyright 	2016 Xibalba Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link 		https://gitlab.com/xibalba/sotol
 */
 
namespace xibalba\sotol\util;

/**
 * Sort class
 * This class provide basic functionality for sort pages.
 * 
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Sort {
	public static function byDateAsc(&$pages) {
		usort($pages, function($pageA, $pageB) {
			$t1 = strtotime($pageA->getMeta('date'));
			$t2 = strtotime($pageB->getMeta('date'));
			return $t1 - $t2;
		});
	}

	public static function byDateDesc(&$pages) {
		usort($pages, function($pageA, $pageB) {
			$t1 = strtotime($pageA->getMeta('date'));
			$t2 = strtotime($pageB->getMeta('date'));
			return $t2 - $t1;
		});
	}
}
