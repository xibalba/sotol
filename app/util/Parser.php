<?php
/**
 * @copyright 	2016 Xibalbá Lab.
 * @license 	http://opensource.org/licenses/bsd-license.php
 * @link 		https://gitlab.com/xibalba/sotol
 */
 
namespace xibalba\sotol\util;

use Michelf\MarkdownExtra;

/**
 * Parser class
 * This class provide methods for process «.md» content files
 * 
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Parser {
	/**
	 *	Loads and parses a text file.
	 *
	 *	First it separates the different blocks into the meta block and the content block
	 * 
	 *	@param string $file
	 *	@return array processed data
	 */
	public static function textFile(string $file) : array {
		// Get file content and normalize line breaks.
		$content = preg_replace('/\r\n?/', "\n", file_get_contents($file));
		$meta = static::getMeta($content);

		// Remove only the optional, leading meta-block comment
		if (strncmp('<!--', $content, 4) === 0) {
			// leading meta-block comment uses the <!-- --> style
			$content = substr($content, max(4, strpos($content, '-->') + 3));
		} elseif (strncmp('/*', $content, 2) === 0) {
			// leading meta-block comment uses the /* */ style
			$content = substr($content, strpos($content, '*/') + 2);
		}

		return [
			'content' => trim($content),
			'meta' => $meta
		];
	}

	/**
	 * Extract meta data of key-value pairs of content text.
	 *
	 * @param string $content
	 * @return array The extrated metadata as array.
	 */
	protected static function getMeta(string $content) : array {
		$start = substr($content, 0, 4);
		if ($start === '<!--') $stop = '-->';
		elseif (substr($start, 0, 2) === '/*') {
			$start = '/*';
			$stop = '*/';
		} else return [];

		$rawMeta = trim(substr($content, strlen($start), strpos($content, $stop) - (strlen($stop) + 1)));

		if (empty($rawMeta)) return [];

		$meta = [];

		// Check if meta is defined as JSON
		if (substr($rawMeta, 0, 1) === '{') $meta = json_decode($rawMeta, true);
		else {
			// Process as «key: value» pairs
			$lines = explode("\n", $rawMeta);
			foreach ($lines as $line) {
				$parts = explode(':', $line, 2);

				if (count($parts) !== 2) continue;

				$parts = array_map('trim', $parts);

				$newKey = strtolower($parts[0]);
				$newKey = preg_replace('/[^\w+]/', '_', $newKey);

				$meta[$newKey] = $parts[1];
			}
		}

		return $meta;
	}

	/**
	 * Parses a text file including markdown syntax.
	 *
	 * @param string $file
	 * @return Array with processed data
	 */
	public static function flatMdFile(string $file) : array {
		$data = self::textFile($file);
		if(!empty($data['content'])) $data['content'] = MarkdownExtra::defaultTransform($data['content']);

		return $data;
	}
}
